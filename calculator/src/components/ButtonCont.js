import '../index.css';

const ButtonCont = ({ children }) => {
  return <div className="buttonCont">{children}</div>;
};

export default ButtonCont;